https://medium.com/crypto-currently/lets-build-the-tiniest-blockchain-e70965a248b


- https://gist.github.com/aunyks/2aae6d164ba7abb1af8017c50af321b3#file-snakecoin-block-py
- https://gist.github.com/aunyks/1ab4f07568b380f8c42636b3ab8d2dcc#file-snakecoin-genesis-py
- https://gist.github.com/aunyks/3887b5b935f2c5774fbea9aa183a78e8#file-snakecoin-new-block-py
- https://gist.github.com/aunyks/db77e77eced412eeddbe17dfab63143d#file-snakecoin-blockchain-py
- https://gist.github.com/aunyks/dd9cf41b8156e75ce5853f2064e3244d#file-snakecoin-transaction-py
- https://gist.github.com/aunyks/3f4372b2d01d8584ae2ccd8bab62c520#file-snakecoin-node-txion-submit-py
- https://gist.github.com/aunyks/0f29a18de1750977b9baa7404da09562#file-snakecoin-pow-py
- https://gist.github.com/aunyks/a01b13e9ffba1d0734cfd19ff02f058d#file-snakecoin-consensus-py

1. Create a transaction.

	curl "localhost:5000/txion" \
	     -H "Content-Type: application/json" \
	     -d '{"from": "akjflw", "to":"fjlakdj", "amount": 3}'

2. Mine a new block.

	curl localhost:5000/mine


