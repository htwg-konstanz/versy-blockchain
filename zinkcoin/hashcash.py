#!/usr/bin/env python3
# -*- coding: utf-8 -*-
###
# Copyright (c) 2019 thomas.zink _at_ uni-konstanz _dot_ de
# Usage of the works is permitted provided that this instrument is retained with the works.
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###
from Crypto.Hash import SHA256


def H(s, token):
    return int(SHA256.new(hex(s).encode('utf-8') + hex(token).encode('utf-8')).hexdigest(), 16)


def mint(s: int, k: int) -> int:
    n = 256
    token = 0
    found = False
    while not found:
        h = H(s, token)
        print("%x" % h)
        if value(h, k):
            found = True
            break
        token += 1
    return token


def value(h: int, k: int) -> bool:
    n = 256
    return h < 2 ** (n - k)


if __name__ == '__main__':
    import time

    start = time.time()
    token = mint(hash("Hello World"), 20)
    lap = time.time() - start
    print("Token %d found after %d secs (%f h/s)" %(token, lap, token/lap))
