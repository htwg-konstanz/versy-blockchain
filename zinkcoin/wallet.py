#!/usr/bin/env python3
# -*- coding: utf-8 -*-
###
# Copyright (c) 2019 thomas.zink _at_ uni-konstanz _dot_ de
# Usage of the works is permitted provided that this instrument is retained with the works.
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###
from Crypto.PublicKey import RSA
from Crypto import Random
from Crypto.Hash import SHA256


class Transaction:
    def __init__(self, sender, receiver, msg, signature):
        self.sender = sender
        self.receiver = receiver
        self.msg = msg
        self.signature = signature

    def __str__(self):
        return "from: %s\nto: %s\nsignature: %s\nmsg: %s" %(self.sender, self.receiver, self.signature, self.msg)

    @property
    def hash(self):
        return int(SHA256.new(self.asBytes).hexdigest(), 16)

    def __hash__(self):
        return self.hash

    @property
    def asBytes(self):
        return self.sender + self.receiver + hex(self.signature[0]).encode('utf-8') + self.msg


class Wallet:
    def __init__(self):
        generator = Random.new().read
        self.key = RSA.generate(1024, generator)

    def send(self, receiver, msg) -> Transaction:
        plaintxt = msg.encode('utf-8')
        rec_key = RSA.importKey(receiver)
        ciphertxt = rec_key.encrypt(plaintxt,73)[0]
        msg_hash = SHA256.new(ciphertxt + receiver).digest()
        sign = self.key.sign(msg_hash,73)
        return Transaction(self.key.publickey().exportKey(),receiver, ciphertxt, sign)

    def receive(self, ta: Transaction):
        if not ta.receiver == self.key.publickey().exportKey():
            print("Incorrect receiver key")
            return
        snd_key = RSA.importKey(ta.sender)
        msg_hash = SHA256.new(ta.msg + ta.receiver).digest()
        verified = snd_key.verify(msg_hash, ta.signature)
        print("%s Signature" % ("GOOD" if verified else "BAD"))
        if not verified:
            return
        plaintxt = self.key.decrypt(ta.msg)
        return plaintxt

    @property
    def address(self):
        return self.key.publickey().exportKey()


def test_wallet():
    alice = Wallet()
    bob = Wallet()
    tom = Wallet()
    klaus = Wallet()

    t1 = alice.send(bob.address, "Hallo Bob")
    t2 = bob.send(alice.address, "Hallo Alice")
    t3 = tom.send(klaus.address, "Hallo Klaus")
    t4 = klaus.send(tom.address, "Klappe")

    print("Wallet 1: %s" % alice.address)
    print("Wallet 2: %s" % bob.address)
    print("Wallet 3: %s" % tom.address)
    print("Wallet 4: %s" % klaus.address)

    print("Transaction 1:\n%s" %t1)
    print("Transaction 2:\n%s" % t2)
    print("Transaction 3:\n%s" % t1)
    print("Transaction 4:\n%s" % t2)

    print(bob.receive(t1))
    print(alice.receive(t2))
    print(klaus.receive(t3))
    print(tom.receive(t4))

    return alice, bob, tom, klaus, t1, t2, t3, t4


if __name__ == '__main__':
    alice, bob, tom, klaus, t1, t2, t3, t4 = test_wallet()
