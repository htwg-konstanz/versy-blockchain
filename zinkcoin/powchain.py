#!/usr/bin/env python3
# -*- coding: utf-8 -*-
###
# Copyright (c) 2019 thomas.zink _at_ uni-konstanz _dot_ de
# Usage of the works is permitted provided that this instrument is retained with the works.
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###
from Crypto.Hash import SHA256
from Crypto import Random
from blockchain import *
from datetime import datetime
import hashcash


class Powblock(Block):
    def __init__(self, prev_hash, data, nonce, ts=None):
        super().__init__(prev_hash, data, ts)
        self.nonce = nonce

    @classmethod
    def fromBlock(cls, block: Block, nonce: int):
        return cls(block.prev_hash, block.data, nonce, block.timestamp)

    @property
    def hash(self) -> int:
        return int(SHA256.new(hex(super().hash).encode('utf-8') + hex(self.nonce).encode('utf-8')).hexdigest(), 16)

    def __str__(self):
        return "{ timestamp: %s, prev_hash: %x, hash: %x, nounce: %s, data: %s }" % (
        self.timestamp, self.prev_hash, self.hash, self.nonce, self.data)


class Powchain(Blockchain):
    k = 20

    def _genesis(self):
        block = proof_of_work(Block(0, "genesis"), self.k)
        list.append(self, block)

    def append(self, block: Powblock) -> None:
        if datetime.fromisoformat(block.timestamp) <= datetime.fromisoformat(self[-1].timestamp):
            print("ERROR: illegal timestamp")
            return
        if not hashcash.value(block.hash, self.k):
            print("ERROR: invalid nonce")
            return
        if not block.prev_hash == self[-1].hash:
            print("ERROR: invalid previous hash")
        list.append(self, block)

    def __str__(self):
        s = ""
        for b in self:
            s = s + str(b) + '\n'
        return s


def proof_of_work(block: Block, k: int) -> Powblock:
    nonce = hashcash.mint(block.hash, k)
    return Powblock.fromBlock(block, nonce)


def test_chain():
    chain = Powchain()
    block = proof_of_work(Block(chain[0].hash, "Hallo"), chain.k)
    print(chain)
    Blockchain.verify(chain)
    return chain


if __name__ == '__main__':
    chain = test_chain()