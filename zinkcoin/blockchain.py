#!/usr/bin/env python3
# -*- coding: utf-8 -*-
###
# Copyright (c) 2019 thomas.zink _at_ uni-konstanz _dot_ de
# Usage of the works is permitted provided that this instrument is retained with the works.
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###
from Crypto.Hash import SHA256
from datetime import datetime


class Block:
    def __init__(self, prev_hash, data, ts=None):
        self.timestamp = datetime.now().isoformat() if not ts else ts
        self.prev_hash = prev_hash
        self.data = data

    @property
    def hash(self) -> int:
        return int(SHA256.new(self.timestamp.encode('utf-8') + hex(self.prev_hash).encode('utf-8') + hex(hash(self.data)).encode('utf-8')).hexdigest(), 16)

    def __hash__(self):
        return self.hash

    def __str__(self):
        return "{ timestamp: %s, prev_hash: %x, hash: %x, data: %s }" % (self.timestamp, self.prev_hash, self.hash, self.data)


class Blockchain(list):
    def __init__(self):
        super().__init__()
        self._genesis()

    def _genesis(self):
        list.append(self, Block(0, "genesis"))

    def append(self, data) -> None:
        list.append(self, Block(self[-1].hash, data))

    @classmethod
    def verify(cls, chain) -> bool:
        prev_hash = chain[0].prev_hash
        chain_verified = True
        for block in chain:
            should = Block(prev_hash, block.data, block.timestamp)
            if block.prev_hash == should.prev_hash:
                print("%s: verification SUCCESSFUL" % block)
            else:
                chain_verified = False
                print("%s: verification FAILED" % block)
            prev_hash = should.hash
        print("chain verification %s" % ("SUCCESSFUL" if chain_verified else "FAILED"))

    def __str__(self):
        s = ""
        for b in self:
            s = s + str(b) + '\n'
        return s


def test_chain():
    chain = Blockchain()
    chain.append("hello")
    chain.append("world")
    chain.append("!")
    print(chain)
    Blockchain.verify(chain)
    return chain


if __name__ == '__main__':
    chain = test_chain()
