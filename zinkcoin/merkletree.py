#!/usr/bin/env python3
# -*- coding: utf-8 -*-
###
# Copyright (c) 2019 thomas.zink _at_ uni-konstanz _dot_ de
# Usage of the works is permitted provided that this instrument is retained with the works.
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###
#from hashlib import sha256
import math
from Crypto.Hash import SHA256

#hashfn = sha256
hashfn = SHA256.new


class MerkleTree(list):
    def __init__(self):
        super().__init__()
        self.tree = []

    def append(self, data) -> None:
        list.append(self, data)

    def build(self):
        if math.log(len(self),2) % 1:
            print("ERROR: number of leaves is not a power of 2. Cannot build tree.")
            return
        for l in range(0, self.levels):
            self.tree.append([])
        self.tree.append(self)
        for l in range(len(self.tree), 1, -1):
            l -= 1
            lvl = self.tree[l]
            for i in range(0, len(lvl), 2):
                h = hashfn(hashfn(lvl[i]).digest() + hashfn(lvl[self.sibling(i)]).digest()).digest()
                p = self.parent(i)
                self.tree[l-1].append(h)

    @property
    def root(self):
        if len(self.tree):
            return self.tree[0][0]

    @property
    def asStr(self):
        s = ""
        for l in self.tree:
            s += str(l) + '\n'
        return s

    @property
    def levels(self):
        return int(math.log(len(self),2))

    @classmethod
    def children(cls, i, l):
        return (2*l*i, 2*l*i+1)

    @classmethod
    def parent(cls, i):
        return int(i/2)

    @classmethod
    def sibling(cls, i):
        return i + (1 - i % 2 * 2)


if __name__ == '__main__':
    from wallet import *
    alice, bob, tom, klaus, t1, t2, t3, t4 = test_wallet()
    t = MerkleTree()
    t.append(t1.asBytes)
    t.append(t2.asBytes)
    t.append(t3.asBytes)
    t.append(t4.asBytes)
    t.build()
    print(t.asStr)