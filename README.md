# Versy Blockchain

Experiments with ethereum blockchain and smart contracts for Versy.

## TODOs




## References

- [Solidity Smart Contract Documentation](https://solidity.readthedocs.io/en/develop/)
- [Ethereum Releases](https://github.com/ethereum/mist/releases)
- [Remix Solidity Browser IDE](https://remix.ethereum.org/):
    To choose which blockchain to connect to, e.g. for deploying a contract, use 
    Run --> environment. JavaScript VM means sandbox execution within the browser.
- [Atom Solidity Linter](https://atom.io/packages/linter-solidity)
- [Etheratom - Atom Solidity IDE](https://github.com/0mkara/etheratom)
- [Hello World Example](https://www.ethereum.org/greeter)
